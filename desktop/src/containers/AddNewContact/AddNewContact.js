import React, {Component} from 'react';
import {createCont} from "../../store/actions/actionContacts";
import {connect} from "react-redux";
import Button from "../../components/UI/Button/Button";


class AddNewContact extends Component {
  state = {
    name: '',
    phoneNumber: '',
    mail: '',
    img: '',
  };

  orderHandler = event => {
    event.preventDefault();

    const contactData = {
      name: this.state.name,
      phoneNumber: this.state.phoneNumber,
      mail: this.state.mail,
      img: this.state.img
    };

    this.props.createCont(contactData, this.props.history);
  };

  valueChanged = event => {
    const {name, value} = event.target;
    this.setState({[name]: value});
  };

  backTo = () => {
      this.props.history.push('/');
  };

  render() {
    return (
        <div className="AddedDishes">
          <h3>Added dishes</h3>
          <form onSubmit={this.orderHandler}>
            <input className="Input" type="text" name="name" placeholder="Name"
                   value={this.state.name} onChange={this.valueChanged}/>
            <input className="Input" type="number" name="phoneNumber" placeholder="Phone number"
                   value={this.state.phoneNumber} onChange={this.valueChanged}/>
            <input className="Input" type="email" name="mail" placeholder="mail"
                   value={this.state.mail} onChange={this.valueChanged}/>
            <input className="Input" type="text" name="img" placeholder="Foto"
                   value={this.state.img} onChange={this.valueChanged}/>
            <Button type='submit' btnType="Success">Save</Button>

          </form>
            <Button onClick={this.backTo} btnType="Success">Back to contacts</Button>
            {this.state.img ? <img style={{width:'200px',height:'200px'}} src={this.state.img} alt="pic"/>:null}
        </div>
    );
  }
}

const mapStateToProps = state => ({
  contacts: state.ord.contacts,
  loading: state.ord.loading,
});

const mapDispatchToProps = dispatch => ({
  createCont: (contactData, history) => dispatch(createCont(contactData,history)),
});


export default connect(mapStateToProps, mapDispatchToProps)(AddNewContact) ;