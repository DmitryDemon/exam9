import React, {Component} from 'react';
import {connect} from "react-redux";
import Spinner from "../../components/UI/Spinner/Spinner";
import ListContacts from "../../components/ListContacts/ListContacts";
import {createCont, createList} from "../../store/actions/actionContacts";

import './Contacts.css';


class Contacts extends Component {


    componentDidMount(){
        this.props.createList()
    }



  render() {
      return (
      <div className="Dishes">
        <h1>Contacts</h1>

        <div className="Dishes-content">
          {this.props.loading ? <Spinner />  : <ListContacts
             contacts={this.props.contacts}
          />}
        </div>

      </div>
    );
  }
}

const mapStateToProps = state =>({
    contacts: state.ord.contacts,
    loading: state.ord.loading,
});
const mapDispatchToProps = dispatch =>({
    createList: () => dispatch(createList()),
    createCont: (contactData, history) => dispatch(createCont(contactData,history)),
});

export default connect(mapStateToProps,mapDispatchToProps)(Contacts);