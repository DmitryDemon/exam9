import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://exam9-dima.firebaseio.com/'
});

export default instance;