import React, { Component } from 'react';
import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Contacts from "./containers/Contacts/Contacts";
import AddNewContact from "./containers/AddNewContact/AddNewContact";


import './App.css';


class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={Contacts} />
                    <Route path="/add-contact" component={AddNewContact} />
                    <Route render={() => <h1>No found!</h1>} />
                </Switch>
            </Layout>
        );
    }
}

export default App;
