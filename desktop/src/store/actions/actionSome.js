
import {ADD_CONTACT, DELETE_CONTACT, EDIT_CONTACT, INIT_CONTACT} from "./actionTypes";

export const addDish = (name, phoneNumber, mail, img) => ({type: ADD_CONTACT, name, phoneNumber, mail, img});//addCont

export const editDish = (name, phoneNumber, mail, img) => ({type: EDIT_CONTACT, name, phoneNumber, mail, img});//editCont

export const deleteDish = (name, phoneNumber, mail, img) => ({type: DELETE_CONTACT, name, phoneNumber, mail, img});//deleteCont

export const initDishes = () => ({type: INIT_CONTACT});//initCont