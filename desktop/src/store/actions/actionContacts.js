import axios from '../../axios-contacts';

import {initDishes} from "./actionSome";
import {CONTACT_FAILURE, CONTACT_REQUEST, CONTACT_SUCCESS} from "./actionTypes";
//initCont

export const contactRequest = () => ({type: CONTACT_REQUEST});
export const contactSuccess = contacts => ({type: CONTACT_SUCCESS, contacts});
export const contactFailure = error => ({type: CONTACT_FAILURE, error});

export const createList = () => {
    return dispatch => {
        dispatch(contactRequest());//contactRequest
        axios.get('contacts.json').then(response => {
                dispatch(contactSuccess(response.data));
            },
            error => dispatch(contactFailure(error))
        );
    }
};
export const createCont = (contactData, history) => {//createContact
    return dispatch => {
        dispatch(contactRequest());//contactRequest

        axios.post('contacts.json', contactData).then(
            response => {
                console.log(response, 'RESPONSE');
                dispatch(createList());
                dispatch(initDishes());//initCont
                history.push('/');
            },
            error => dispatch(contactFailure(error))
        );
    }
};
export const deleteContact = (contactData) => {
    return dispatch => {
        dispatch(contactRequest());
        axios.delete('contacts/' + contactData + '.json').then(
            response => {
                dispatch(createList());
            },
            error => dispatch(contactFailure(error))
        );
    }
};

export const editContact = (contactData) => {
    return dispatch => {
        dispatch(contactRequest());
        axios.put('contacts/' + contactData.id + '.json', contactData).then(
            response => {
                dispatch(createList());
            },
            error => dispatch(contactFailure(error))
        )
    }
};