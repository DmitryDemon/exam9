import {ADD_CONTACT, DELETE_CONTACT, EDIT_CONTACT, INIT_CONTACT} from "../actions/actionTypes";

const initialState = {
  contacts: {},
  loader: false,
  error: null,
};

const reducerSome = (state = initialState, action) => {
  switch (action.type) {
    case ADD_CONTACT:
    case EDIT_CONTACT:
    case DELETE_CONTACT:
    case INIT_CONTACT:
    default:
      return state;
  }
};

export default reducerSome;