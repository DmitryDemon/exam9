import {CONTACT_FAILURE, CONTACT_REQUEST, CONTACT_SUCCESS} from "../actions/actionTypes";

const initialState = {
    contacts: {},
    loading: false,
    error: null,
    ordered: false,
};

const reducerContact = (state = initialState, action) => {
    switch (action.type) {
        case CONTACT_REQUEST:
            return {...state, loading: true};
        case CONTACT_SUCCESS:
            return {...state, loading: false, contacts: action.contacts};
        case CONTACT_FAILURE:
            return {...state, loading: false, error: action.error};
        default:
            return state;
    }
};

export default reducerContact;