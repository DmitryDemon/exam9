import React from 'react';

import './Logo.css';


const Logo = () => (
  <div className="Logo">
    <h1>Phone Book</h1>
  </div>
);


export default Logo;