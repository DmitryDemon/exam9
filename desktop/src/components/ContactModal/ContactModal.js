import React,{Component} from 'react';
import Modal from "../UI/Modal/Modal";
import Button from "../UI/Button/Button";
import './ContactModal.css'


class ContactModal extends Component {
    state = {
        name: this.props.name,
        phoneNumber: this.props.phoneNumber,
        mail: this.props.mail,
        img: this.props.img,
        formOff: false,
    };

    formOn = () => {
        this.setState({formOff: true});
    };



    contactHandler = event => {
        event.preventDefault();

        const contactData = {
            name: this.state.name,
            phoneNumber: this.state.phoneNumber,
            mail: this.state.mail,
            img: this.state.img
        };

        this.props.createOrder(contactData, this.props.history);
    };

    valueChanged = event => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    render() {
        return (
            <Modal
                show={this.props.show}
                close={this.props.off}
            >
                <div  className='wrapper'>
                    <Button onClick={this.props.off} btnType='Danger' className='btn close'>X</Button>
                    <img className='img' src={this.props.img} alt="pic"/>
                    <h1 className='name'>{this.props.name}</h1>
                    <p className='phone'><img style={{width:'30px',height:'30px'}} src="https://st2.depositphotos.com/4845131/7223/v/950/depositphotos_72231815-stock-illustration-icon-phone-tube.jpg" alt=""/>{this.props.phoneNumber}</p>
                    <a href='https://www.google.com/search?q=%D0%B3%D1%83%D0%B3%D0%BB&oq=ueuk&aqs=chrome.1.69i57j0l5.2327j0j8&sourceid=chrome&ie=UTF-8' className='mail'><img style={{width:'30px',height:'30px'}} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAYFBMVEX///9kZGRhYWHd3d3j4+NycnJ+fn53d3fOzs7W1tZubm5eXl77+/vt7e1mZmb29vaZmZnIyMjp6emGhoaMjIyzs7NZWVmoqKjy8vKioqK/v7/Ly8uSkpJ7e3uKioqwsLBa/kARAAAFnElEQVR4nO2c65KiMBBGSUAUwh0FRdT3f8sNO1uzYzoqSkKytd/5OVWjHEg63elIEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwD9JmTeRVzRNXpoUTPpx38Y+0e7HPjQnmG067h/d1ZxhUHXMP7rKoGF+Fa59CF2bGzQM8j13baTAq6NJwSA4ngqfHHlxMxpKJ8pD6o+i2A6ZaUFJvfVlMop9YsFPkrReKHLWN3YEgyCqPFAU29r4FPzLsReuJ6PYR/b8JNngNqTKGGojxNyxuzhU5BcLIYbcsmjvajLyVLPKL3+k4Vn9jGPFnTxGEdNF8Fgvn5XnLckenCz+nI+0Wgqr7fJhu0sZGRtZvVl7pPLiQBPtOhZGDDlvyVBI4nUVxSYhI7Ts5aWZeYaMb3fqn/M18xvetXSRj1oZDkwZykTpQG7habV4w9OBXFY5FNMtNmYo53lPpsFtnXjDiz0NMc3pK7kyZyhnAg1l9RqTkac3GmKS+M+2iklDuRyRyRjar/zlt5L5kR++U0ejhjKekenQVJYVu4oWSmHFvr/VrOE0YMjif7OpyIuBxNBs+JkYGzactn/InKg7a47dhYaY7H6/yLghE/RLI0shlRe9ZhFM74ObeUM5Ukkm3lyZBUce1+RaykEdMBYMGRcH9dbm5ncaOato0aDZRLFhKEfqSY1vxosNsSG3Uab7muXXjiGjdUx23phU1O0W5r3uLloylHOELv7mNlO1u4WhfrLbMtTlwsfWUH9KFDXdmRgeTANrhjLeVOplZL2JYkMWozQNPY6PPtqeoVyOY3Ilj270O4JpT64g2z2eATYNmdiSsnsXL1PkrCUTXObZT26cVUPGNyRrDMcl8YanZB2ShdLTnT27hlMmru5RNafP4422ITFcnt4zy4YydSTFTXn4tLPRaRoSef8iW7JtOK2M6mVl9UcpHO80Td2kffVR9g1lcKCL//OBpUVs6JVmh9f3agXD6eaTYmN800+OdtqQaMYZU3oNQ11ZfHw1fdTLpA2JbJfOGQnrGOoy8XfajGLU5Nm3eQnSSoZM0Gp19rEGXuh2C8eZN2gtQ7n4H9T/C+cdoxIXultYDrO7sKsZTrFC/cdmxrEGmcBrFsFq/hBfz1B+11UdbOXLYoMXmoM/yTvF9JqGuiXtebEhCyXNbuHtrd3JVQ2nslh9IsmTxV/m2ZrdwjcPCaxrOIVFdeGOHnY2eKxpmdXvpkMrG06Lvxo4Huw0ysikaZlpN5v8MmSiVb9Ru9Mo0oGmabvr+9sg6xtORZ7yAVlNgqOIaVc+OHxy+sGBoQwgdPG/v3beVTTE5ONHrQEXhvIJkUy8/Fnn8fRMv+X84fEVN4aso+vcd7zhhbaU/3Qj0pGhXPxJWTx8TUauaUgEyecn5VwZ6pq3v3caxVWXZy84YOXMcOo9qJMx3ItO05CITku6j+4MJSNd/DW7hbuXm03+GtLTrtQvW3rqyKmhHHw087ynaZceG3drKJcNzVmDv2Rnsbjn6NqQdePjX1/lvYGGo3NDTSb+hywx8nM494Yy3mg6ulPFsTXS+PfAUHdUbDo3Yuh8ig+GU7GrKtZmHiDzxHC6jPt4Y/B0sSeGjPMf1xGaPHfri+GPTHxRnq35YF8Mp4OGv0fqsjxb87neGLKvTLw2/RsGrwx5u7vN6gm+9aE+Gcr63vw5VL8MbQBDGMLQPTCEIQzdA0MYwtA9MIQhDN3zHxgaeCVPYmx/2gY8Xv4CzGhZG9oy4kn7bi7ZyYM3mD2E05+6vc/Zo7cJqpgINBLbb4ZYgJFHGAT5yu8Tmo+4Gnp5W+SpojD3jtaoKoTrtwYTREpf0vE5ZV3FG7+IdSeslnCMQr+ILL4AEwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAArPILTh91rqb6MDEAAAAASUVORK5CYII=" alt=""/>{this.props.mail}</a>
                    <Button onClick={this.formOn} className='btn edit' btnType="Warning">Edit</Button>
                    <Button onClick={()=>this.props.delete(this.props.cont)} className='btn delete'  btnType="Danger">Delete</Button>
                    {this.state.formOff ?
                        <div>
                            <form onSubmit={()=>this.props.edit({...this.state, id: this.props.cont})}>
                                <input className="Input" type="text" name="name" placeholder="Name"
                                       value={this.state.name} onChange={(e)=>this.valueChanged(e)}/>
                                <input className="Input" type="number" name="phoneNumber" placeholder="Phone number"
                                       value={this.state.phoneNumber} onChange={(e)=>this.valueChanged(e)}/>
                                <input className="Input" type="email" name="mail" placeholder="mail"
                                       value={this.state.mail} onChange={(e)=>this.valueChanged(e)}/>
                                <input className="Input" type="text" name="img" placeholder="Foto"
                                       value={this.state.img} onChange={(e)=>this.valueChanged(e)}/>
                                <Button type='submit' btnType="Success">SaveForm</Button>
                            </form>
                        </div>

                        :null
                    }

                    {console.log(this.state)}
                </div>
            </Modal>
        )
    }
}

export default ContactModal;