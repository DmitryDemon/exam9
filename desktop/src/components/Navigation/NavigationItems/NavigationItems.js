import React from 'react';

import NavigationItem from "./NavigationItem/NavigationItem";

import './NavigationItems.css';

const NavigationItems = () => (
  <ul className="NavigationItems">
    <NavigationItem to="/" exact>Contacts</NavigationItem>
    <NavigationItem to="/add-contact">Add new contact</NavigationItem>
  </ul>
);


export default NavigationItems;