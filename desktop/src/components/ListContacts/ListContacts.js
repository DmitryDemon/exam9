import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import {deleteContact, editContact} from "../../store/actions/actionContacts";
import ContactModal from "../ContactModal/ContactModal";
import './ListContacts.css';



class ListContacts extends Component {
    state = {
        contactMod: false,
        contactOne:'',
    };
    contactOn = id => {
        this.setState({contactMod: id});
    };

    contactCancel = () => {
        this.setState({contactMod: false});
    };


    render() {
      if (this.props.contacts === null){
            return <h1>Add dishes!</h1>
        } else  {

            return (
                Object.keys(this.props.contacts).map((cont,key) =>{
                    const oneCont = this.props.contacts[cont];
                    return (
                        <Fragment key={key}>
                        <div onClick={()=>this.contactOn(cont)} className='ListDishes'>
                            <img src={oneCont.img} alt="cont"/>
                            <p className="ListDishes-title">{oneCont.name}</p>
                        </div>
                        <ContactModal
                            on={this.contactOn}
                            off={this.contactCancel}
                            show={this.state.contactMod === cont}

                            oneCont={oneCont}
                            cont={cont}
                            name={oneCont.name}
                            phoneNumber={oneCont.phoneNumber}
                            mail={oneCont.mail}
                            img={oneCont.img}
                            delete={(cont)=>this.props.deleteContact(cont)}
                            edit={(cont)=>this.props.editContact(cont)}
                        />
                    </Fragment>
                    )
                })
            );
        }
    }
}
const mapStateToProps = state =>({
    contacts: state.ord.contacts,
    loading: state.ord.loading,
});
const mapDispatchToProps = dispatch =>({
    deleteContact: (orderContact) => dispatch(deleteContact(orderContact)),
    editContact: (orderContact) => dispatch(editContact(orderContact)),
});

export default connect(mapStateToProps,mapDispatchToProps) (ListContacts);

